// This is student specific section of the app. It is authenticated and runs on the client
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import { Router } from '@reach/router'
import Layout from '../components/layout'
import { login, isAuthenticated } from '../utils/auth'

const Dashboard = () => <p>This is the student dashboard</p>
const Courses = ({ children }) => (
  <div>
    <h2>Courses</h2>
    <ul>
      <li>
        <Link to="/student/courses/ALG-101">Linear Algerbria 101</Link>
      </li>
      <li>
        <Link to="/student/courses/PHY-101">Physics 101</Link>
      </li>
    </ul>
    {children}
  </div>
)
Courses.propTypes = {
  children: PropTypes.object.isRequired,
}

const Course = ({ courseId }) => (
  <div>
    <h2>Course {courseId}</h2>
  </div>
)
Course.propTypes = {
  courseId: PropTypes.string.isRequired,
}

const CourseIndex = () => (
  <div>
    <p>Please choose a course.</p>
  </div>
)
const Student = () => {
  if (!isAuthenticated()) {
    login()
    return <p>Redirecting to login...</p>
  }

  return (
    <>
      <Layout>
        <nav>
          <Link to="/student/">My Dashboard</Link> <br />
          <Link to="/student/courses">My Courses</Link>
          <br />
        </nav>
        <Router>
          <Dashboard path="/student/*" />
          <Courses path="/student/courses">
            <CourseIndex path="/" />
            <Course path=":courseId" />
          </Courses>
        </Router>
      </Layout>
    </>
  )
}

export default Student
