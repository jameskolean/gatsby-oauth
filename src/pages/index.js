import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>NohinGo</h1>
    <p>Welcome to NohinGo.</p>
    <p>The fun way to instruct and learn.</p>
    <Link to="/welcome/">Go to Welcome Page</Link>
    <br />
    <Link to="/student/">Student Page</Link>
  </Layout>
)

export default IndexPage
