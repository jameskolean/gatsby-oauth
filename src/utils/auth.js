import auth0 from 'auth0-js'

export const isBrowser = typeof window !== 'undefined'

const tokens = {
  idToken: false,
  accessToken: false,
}

const user = {}

export const isAuthenticated = () => tokens.idToken !== false

const auth = isBrowser
  ? new auth0.WebAuth({
      domain: process.env.AUTH0_DOMAIN,
      clientID: process.env.AUTH0_CLIENTID,
      redirectUri: process.env.AUTH0_CALLBACK,
      responseType: 'token id_token',
      scope: 'openid profile email',
    })
  : {}

export const login = () => {
  if (!isBrowser) {
    return
  }
  auth.authorize()
}

export const logout = () => {
  tokens.accessToken = false
  tokens.idToken = false
  user.name = ''
  user.nickname = ''
  user.picture = ''
  window.localStorage.setItem('isLoggedIn', false)

  auth.logout({
    returnTo: window.location.origin,
  })
}

const setSession = (cb = () => {}) => (err, authResult) => {
  if (err) {
    if (err.error === 'login_required') {
      login()
    }
  }
  if (authResult && authResult.accessToken && authResult.idToken) {
    tokens.idToken = authResult.idToken
    tokens.accessToken = authResult.accessToken

    auth.client.userInfo(tokens.accessToken, (_err, userProfile) => {
      user.nickname = userProfile.nickname
      user.name = userProfile.name
      user.picture = userProfile.picture
      window.localStorage.setItem('isLoggedIn', true)

      cb()
    })
  }
}

export const checkSession = callback => {
  const isLoggedIn = window.localStorage.getItem('isLoggedIn')
  console.log(`checkSession isLoggedIn: ${isLoggedIn}`)
  if (isLoggedIn === 'false' || isLoggedIn === null) {
    callback()
  }
  const protectedRoutes = [`/student`, `/welcome`]
  const isProtectedRoute = protectedRoutes
    .map(route => window.location.pathname.includes(route))
    .some(route => route)
  if (isProtectedRoute) {
    auth.checkSession({}, setSession(callback))
  }
}

export const handleAuthentication = callback => {
  auth.parseHash(setSession(callback))
}

export const getProfile = () => user
